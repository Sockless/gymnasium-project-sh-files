#adding support for nfs filesystem
sudo apt install nfs-common

sudo mkdir -p /srv/steamcache/data/
# The -p in this instance means parents. meaning if you want to create a folder with the path ~/new/folder/ it will create the folder "new" and put the folder "folder" inside it. basically if the path described doesn't exist, it will create it.
sudo chmod -R 777 /srv/steamcache/data/
# The -R stands for recursive, so all changes will apply to all folders under the folder in the path

# Editing the ip of server (only useful in ubuntu server)
# sudo nano /etc/netplan/50-cloud-init.yaml

#template for what to write is located here https://askubuntu.com/questions/961552/need-example-netplan-yaml-for-static-ip#961759

sudo apt install curl

#installing docker
sudo apt curl -sSL https://get.docker.com/ | sh

#adding the user to docker group
sudo usermod -aG docker <username>

#Stopping a serivce with a conflicting port bind
sudo systemctl disable avahi-daemon
#actually this might be it

#installing steamcache docker image
sudo docker run --name steamcache --restart=always -d -v /srv/steamcache/data:/data/cache -p 80:80 steamcache/steamcache:latest



#installing steamcache-dns docker image
sudo docker run --name steamcache-dns --restart=always -d -p 53:53/udp -e STEAMCAHCE_IP=<server-ip> steamcache/steamcache-dns:latest

sudo docker run --name steamcache-dns --restart=always -d -p <ipadress>:53:53 -e STEAMCAHCE_IP=<Ipadress> steamcache/steamcache-dns:latest

#watching steamcahce activity
sudo docker exec steamcahce /scripts/watchlog.sh